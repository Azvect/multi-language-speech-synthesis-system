import os
import wave

audio_path = "audio_files/"
aligned_data_path = "aligned_data/"

if not os.path.exists(aligned_data_path):
    os.makedirs(aligned_data_path)

for file in os.listdir(audio_path):
    if file.endswith(".wav"):
        audio_file = wave.open(audio_path + file, "r")
        frames = audio_file.getnframes()
        duration = frames / float(audio_file.getframerate())
        audio_file.close()
        with open(audio_path + file.split(".")[0] + ".txt", "r") as f:
            transcript = f.read()
        words = transcript.split(" ")
        word_times = []
        start = 0.0
        end = 0.0
        for word in words:
            end += (len(word) / len(transcript)) * duration
            word_times.append([word, start, end])
            start = end
        with open(aligned_data_path + file.split(".")[0] + ".align", "w") as outfile:
            for word in word_times:
                outfile.write(word[0] + " " + str(word[1]) + " " + str(word[2]) + "\n")

print("Data alignment complete!")