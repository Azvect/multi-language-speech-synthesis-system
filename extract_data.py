import os
import tarfile

file_path = "audio_files/common_voice.tar.gz"
with tarfile.open(file_path, "r:gz") as tar:
    tar.extractall("audio_files/")

print("Data extraction complete!")