import os
import random
import torch
from torch import nn
from torch.utils.data import DataLoader
from torch.optim import Adam
from dataset import SpeechDataset

aligned_data_path = "aligned_data/"
model_path = "trained_model/"
if not os.path.exists(model_path):
    os.makedirs(model_path)
dataset = SpeechDataset(aligned_data_path)
dataloader = DataLoader(dataset, batch_size=32, shuffle=True)

class SpeechModel(nn.Module):
    def __init__(self):
        super(SpeechModel, self).__init__()
        self.fc1 = nn.Linear(40, 32)
        self.fc2 = nn.Linear(32, 32)
        self.fc3 = nn.Linear(32, len(dataset.vocab))

    def forward(self, x):
        x = self.fc1(x)
        x = torch.relu(x)
        x = self.fc2(x)
        x = torch.relu(x)
        x = self.fc3(x)
        return x

model = SpeechModel()
criterion = nn.CrossEntropyLoss()
optimizer = Adam(model.parameters())
num_epochs = 100

for epoch in range(num_epochs):
    total_loss = 0
    for data in dataloader:
        inputs, labels = data
        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        total_loss += loss.item()
    print("Epoch: {}/{} Loss: {}".format(epoch+1, num_epochs, total_loss/len(dataloader)))
    
torch.save(model.state_dict(), model_path + "speech_model.pth")

print("Training complete!")