# Multi-Language Speech Synthesis System



## Proposal & Overview

The goal of this project is to build a speech synthesis system that can generate speech in multiple languages. The system will be trained on a dataset of audio recordings and corresponding transcriptions in multiple languages. The project will involve the following steps:
1. Identifying and gathering a suitable dataset of audio recordings and corresponding transcriptions in multiple languages. This can be done by scraping websites such as YouTube or Common Voice.
2. Developing a script to automatically extract the audio recordings and transcriptions from the dataset.
3. Aligning the audio recordings with the transcriptions using text mining techniques.
4. Training a speech synthesis model using a framework such as Coqui-TTS on the aligned dataset.
5. Developing a script to generate speech in different languages using the trained model.
6. Documenting all of the steps and processes used in the project in a text file.

This project will contribute to the field of natural language processing by building a multi-language speech synthesis system that can be used to generate speech in multiple languages. The project will also demonstrate the ability to gather, process, and use large datasets in order to train neural network models in the field of speech synthesis.

## Dataset

The dataset used in this project is a collection of audio recordings and corresponding transcriptions in multiple languages. The dataset was gathered from a variety of sources, including YouTube and the Common Voice website. The following steps were taken to gather the dataset:

1. Identifying websites that contain videos or audios in the languages of interest.
2. Scraping the websites to extract the audio recordings and corresponding transcriptions.
3. Preprocessing the dataset to remove any duplicate or irrelevant samples.

The dataset contains a diverse set of voices and accents in each language, which will help to improve the robustness of the speech synthesis system. The number of samples in each language varies depending on the availability of data. The dataset is in a text format and it's a common format that can be used in different scripts. The dataset will be used to train the speech synthesis model and the generated speech quality may vary depending on the quality and size of the dataset used for training.

## Preprocessing

Before the speech synthesis model can be trained, the dataset of audio recordings and corresponding transcriptions must be preprocessed. The following steps were taken to preprocess the dataset:

1. Extracting the audio recordings and transcriptions from the dataset.
2. Aligning the audio recordings with the transcriptions using text mining techniques.
3. Removing duplicate or irrelevant samples from the dataset.

The audio recordings and transcriptions were extracted from the dataset using a script that automatically performs the extraction process. The script was developed using Python and it's designed to be easy to use for different users.

The audio recordings and transcriptions were then aligned using text mining techniques such as dynamic time warping (DTW) and forced alignment. These techniques ensure that the audio recordings and transcriptions are correctly aligned, which is crucial for training the speech synthesis model.

Finally, duplicate or irrelevant samples were removed from the dataset to ensure that the training process is as efficient as possible. The preprocessing process is crucial for the final quality of the generated speech, where the quality of the generated speech depends on the quality of the dataset. The preprocessed dataset was saved in a common format that can be used in different scripts.

## Training

The training process for this project involves using a dataset of audio recordings and corresponding transcriptions to train a speech synthesis model. The following steps were taken to train the model:

1. Preprocessing the dataset by aligning the audio recordings with the transcriptions using text mining techniques.
2. Using the Coqui-TTS framework to train the speech synthesis model on the preprocessed dataset.
3. Fine-tuning the model using a smaller dataset of audio recordings and transcriptions to improve the quality of the generated speech.

The Coqui-TTS framework was chosen for this project because it is a state-of-the-art open-source framework for training speech synthesis models. It is also relatively easy to use and has pre-trained models that can be fine-tuned for specific use cases. The training process was performed on a computing power machine to handle the large dataset and to speed up the training process. During the training process, different configurations and parameters were used to evaluate the effect of them on the final model. After the training process, the model was saved and ready to be used in the generation process.

## Generation

Once the speech synthesis model has been trained, it can be used to generate speech in different languages. The following steps were taken to generate speech using the trained model:

1. Developing a script that uses the trained model to generate speech in different languages.
2. Using the script to generate speech samples in each language supported by the model.
3. Evaluating the quality of the generated speech and fine-tuning the model as necessary.

The script for generating speech was developed using Python and it was designed to be easy to use for different users. It takes the text as an input and returns the generated speech in the form of an audio file.
The generated speech quality may vary depending on the quality and size of the dataset used for training. The script also includes a feature to select the speaker and the accent of the generated speech. The script can be used in different applications such as language learning and accessibility technology. The generated speech can be saved in different audio formats and it's easy to use it in different platforms.

## Documentations

This project includes a detailed documentation of all the steps and processes used in the project. The documentation includes information on the following topics:

1. Dataset: Information on the dataset used in the project, including the sources from which it was gathered and the number of samples in each language.
2. Preprocessing: A description of the steps taken to extract the audio recordings and align them with the transcriptions, including the specific techniques used for text mining and the code used to implement them.
3. Training: A description of the training process, including the speech synthesis framework used and the parameters and configurations used for training the model.
4. Generation: A description of the script used to generate speech in different languages, including information on the specific languages and speakers supported by the system.
5. Usage: Clear instructions on how to use the speech synthesis system, including information on the dependencies required and the specific steps to be taken to run the system.
6. Troubleshooting: A list of common issues that may arise while using the system and suggestions on how to resolve them.

The documentation is provided in a text file within the repository, it's easy to understand and follow. This documentation ensures that the project is easily reproducible and can be used and expanded upon by others in the future.

## Usage

### Dependencies

To use the speech synthesis system, you will need to have the following dependencies installed:

* Python 3.x
* Coqui-TTS library
* Other libraries used in the script such as numpy, pandas etc.

### Packages

Run this code to install packages: **`pip3 install -r requirments.txt`**

### Getting started

1. Clone the repository to your local machine.
2. Download the dataset from Common Voice and unzip the files in the repository.
3. Run the preprocessing script **`preprocessing.py`** to extract the audio recordings and align them with the transcriptions.
4. Run the training script **`train.py`** to train the speech synthesis model on the aligned dataset.
5. Run the generation script **`generate.py`** to generate speech in the desired language.
6. Use the generated speech as per your requirement.

### Additional configurations

You can also change the configurations like language, speaker, gender, etc. in the script.

### Output

The script will generate a wav file with synthesized speech.

### Note

The generated speech quality may vary depending on the quality and size of the dataset used for training. In case of any issues or for further improvements, you can refer to the documentation file for troubleshooting.

## Conclusion

This project aimed to build a multi-language speech synthesis system that can generate speech in multiple languages. The system was trained on a dataset of audio recordings and corresponding transcriptions in multiple languages. The following key steps were taken to achieve this goal:

1. Gathering a suitable dataset of audio recordings and corresponding transcriptions in multiple languages by scraping websites such as YouTube and Common Voice.
2. Developing a script to automatically extract the audio recordings and transcriptions from the dataset.
3. Aligning the audio recordings with the transcriptions using text mining techniques.
4. Training a speech synthesis model using the Coqui-TTS framework on the aligned dataset.
5. Developing a script to generate speech in different languages using the trained model.
6. Documenting all of the steps and processes used in the project in a text file.

The project successfully demonstrated the ability to gather, process, and use large datasets in order to train neural network models in the field of speech synthesis. The resulting speech synthesis system has the potential to be used in various applications such as language learning and accessibility technology. However, the quality of the generated speech can vary depending on the quality and size of the dataset used for training, thus, further improvements can be made by using a more diverse dataset and fine-tuning the model. Additionally, the project also highlighted the importance of proper documentation and the need for clear usage instructions for reproducibility and ease of use.