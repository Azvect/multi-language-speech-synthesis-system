import os
import requests

language = "en"
base_url = "https://voice-prod-bundler-ee1969a6ce8178826482b88e843c335139bd3fb4.s3.amazonaws.com/cv-corpus-4-2019-12-10/en.tar.gz"
if not os.path.exists("audio_files"):
    os.makedirs("audio_files")

audio_file = requests.get(base_url)
open("audio_files/common_voice.tar.gz", "wb").write(audio_file.content)

print("Data gathering complete!")