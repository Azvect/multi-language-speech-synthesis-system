import torch
import torch.nn.functional as F
from dataset import SpeechDataset

model_path = "trained_model/speech_model.pth"
model = SpeechModel()
model.load_state_dict(torch.load(model_path))
model.eval()
dataset = SpeechDataset(aligned_data_path)

def generate_speech(text):
    text = dataset.text_to_sequence(text)
    text = torch.LongTensor(text).view(-1, 1)
    with torch.no_grad():
        speech = model(text)
        speech = F.softmax(speech, dim=1)
        speech = torch.argmax(speech, dim=1)
        speech = dataset.sequence_to_speech(speech)

    return speech

# Testing the function
generated_speech = generate_speech("Hello, how are you today?")
print(generated_speech)